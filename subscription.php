<?php

namespace DS\Development_Layout\Export;

\Bitrix\Main\Loader::includeModule("iblock");

use DS\Development_Layout\Repo\Flat;
use DS\Development_Layout\User\User;
use DS\Development_Layout\User\Event;

use DS\Development_Layout\Repo\Project;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\{Font, Border, Alignment};

class Subscription
{
    const FILE_PATH = '/upload/user_events.xlsx';

    public function process()
    {
        $result = [];

        $users = self::getAllusers();

        $city = self::getAllCity();

        $projects = self::getAllProjects();

        $events = Event::getAllValue();

        if (!empty($events))
        {
            $externalUsers = User::getByPhone(array_keys($users));
            foreach ($externalUsers as $externalUser) {
                if($users[$externalUser['UF_PHONE']]){
                    $users[$externalUser['UF_PHONE']]['DDY_FIO'].= $externalUser['UF_FIO'];
                }
            }

            foreach ($events as $event)
            {
                $phone = User::sanitizePhone($event['UF_USER_PHONE']);

                if(!empty($users[$phone]['EMAIL']))
                {
                    $result[$event['ID']] = [
                        'ID' => $event['ID'],
                        'EMAIL' => $users[$phone]['EMAIL'],
                        'NAME' => $users[$phone]['NAME'],
                        'DDY_FIO' => $users[$phone]['DDY_FIO'],
                        'CITY' => $city[$projects[$event['UF_PROJECT_ID']]['CITY']],
                        'PROJECT_CODE' => $projects[$event['UF_PROJECT_ID']]['CODE'],
                        'PROGRES_SUBSCRIBE' => $event['UF_PROGRES_SUBSCRIBE'],
                        'EVENT_SUBSCRIBE' => $event['UF_EVENT_SUBSCRIBE'],
                        'NEWS_SUBSCRIBE' => $event['UF_NEWS_SUBSCRIBE'],
                    ];
                }
            }
        }
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Email');
        $sheet->setCellValue('B1', 'ФИО');
        $sheet->setCellValue('C1', 'Дольщики');
        $sheet->setCellValue('D1', 'Город');
        $sheet->setCellValue('E1', 'Проект');
        $sheet->setCellValue('F1', 'О новостях ЖК');
        $sheet->setCellValue('G1', 'О мероприятиях');
        $sheet->setCellValue('H1', 'О ходе строительства');

        $line = 2;
        foreach ($result as $item) {
            $sheet->setCellValue('A'.$line, $item['EMAIL']);
            $sheet->setCellValue('B'.$line, $item['NAME']);
            $sheet->setCellValue('C'.$line, $item['DDY_FIO']);
            $sheet->setCellValue('D'.$line, $item['CITY']);
            $sheet->setCellValue('E'.$line, $item['PROJECT_CODE']);
            $sheet->setCellValue('F'.$line, $item['PROGRES_SUBSCRIBE']);
            $sheet->setCellValue('G'.$line, $item['EVENT_SUBSCRIBE']);
            $sheet->setCellValue('H'.$line, $item['NEWS_SUBSCRIBE']);

            $line+= 1;
        }

        $sheet->getStyle('A:H')->applyFromArray([
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ]
        ]);

        try {
            $writer = new Xlsx($spreadsheet);
            $writer->save($_SERVER['DOCUMENT_ROOT']. self::FILE_PATH);

        } catch (PhpOffice\PhpSpreadsheet\Writer\Exception $e) {
            echo $e->getMessage();
        }
        return self::FILE_PATH;
    }

    public function getAllCity()
    {
        $arResult = [];
        $filter = array(
            "IBLOCK_ID" => getIblockIdByCode('info', 'city'),
            "ACTIVE" => 'Y',
        );
        $iterator = \CIBlockElement::GetList([], $filter, false, false, ['ID', 'NAME']);
        while ($element = $iterator->fetch()) {
            $arResult[$element["ID"]] = $element['NAME'];
        }
        return $arResult;
    }

    public function getAllusers()
    {
        $result = [];
        $filter = ['!UF_EMAIL' => false];
        $select = [
            'SELECT' => [
                'UF_EMAIL',
                'UF_PROGRES_SUBSCRIBE',
                'UF_EVENT_SUBSCRIBE',
                'UF_NEWS_SUBSCRIBE',
            ],
        ];
        $rsUsers = \CUser::GetList(($by="personal_country"), ($order="desc"), $filter, $select);
        while($arUser = $rsUsers->Fetch())
        {
            $result[$arUser['LOGIN']] = [
                'ID' => $arUser['ID'],
                'EMAIL' => $arUser['UF_EMAIL'],
                'NAME' => $arUser['NAME'],
            ];

        }

        return $result;
    }

    public function getAllProjects()
    {
        $arResult = [];
        $projects = Project::getAll();

        if(!empty($projects))
        {
            foreach ($projects as $project)
            {
                $arResult[$project['ID']] = $project;
            }
        }
        return $arResult;
    }
}
